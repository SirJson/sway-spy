#!/usr/bin/env python3

import sys
import json
from collections import namedtuple
from sh import swaymsg
from PyQt5.QtCore import (Qt)
from PyQt5.QtWidgets import QWidget, QMainWindow, QTreeView, QApplication, QVBoxLayout
from PyQt5.QtGui import QStandardItemModel, QStandardItem, QIcon


TreeItem = namedtuple('TreeItem', ['key', 'value', 'row'])


def flatNodes(inData: dict) -> list:
        for key, value in inData.items():
            if key is 'node':
                return value



class Example(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.setGeometry(10, 10, 800, 600)
        self.setWindowTitle('Sway Spy')
        self.dataView = QTreeView()
        model = self.windowTreeModel()
        self.dataView.setModel(model)
        self.mainLayout = QVBoxLayout()
        self.mainLayout.addWidget(self.dataView)
        self.setLayout(self.mainLayout)
        self.show()

    def loadWindowTree(self) -> dict:
        print("Fetching current window tree...")
        result = swaymsg('-t', 'get_tree')
        return json.loads(str(result))

    """
    QStandardItemModel model;

    QStandardItem *parentItem = model.invisibleRootItem();

    for (int i = 0; i < 4; ++i) {

    QStandardItem *item = new QStandardItem(QString("item %0").arg(i));

    parentItem->appendRow(item);

    parentItem = item;

    }
    """

    def addData(self, parent: QStandardItem, key: str, value: str, depth: int) -> QStandardItem:
        a = QStandardItem(str(key))
        b = QStandardItem(str(value))
        parent.insertRow(depth, [a, b])
        return a

    def windowTreeModel(self) -> QStandardItemModel:
        model = QStandardItemModel()
        data = self.loadWindowTree()
        root = model.invisibleRootItem()
        pdict(data, 0)
        return model

    def addBranch(self, root, data) -> QStandardItemModel:
        leaf = 0
        if type(data) is not dict:
            return root

        for k, v in data.items():
            if type(v) is dict:
                parent = self.addData(root, TreeItem(key=k, value='', row=leaf))
                self.addBranch(parent, v)
            else:
                self.addData(root, TreeItem(key=k, value=v, row=leaf))
        leaf = leaf + 1

def plist(l: list, depth: int):
    for a in l:
        valType = type(a)
        if valType is dict:
            pdict(a, depth + 1)
        else:
            tabs = '-' * depth
            print('{} {}'.format(tabs, a))

def pdict(d: dict, depth: int):
    for k, v in d.items():
        valType = type(v)
        if valType is list:
            newdepth = depth + 1
            plist(v, newdepth)
        elif valType is dict:
            pdict(v, depth + 1)
        else:
            tabs = '-' * depth
            
            print('{} {} => {}'.format(tabs, k, v))


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
